package com.szymanczyk.machnicki.burgerit.cart

import com.szymanczyk.machnicki.burgerit.R

class CartFragmentPresenter(val view: View) {

    fun updateCart() {
        val amount = Cart.getAmount()
        val deliveryPrice = 0.0
        view.displayDeliveryPrice(deliveryPrice)
        view.displayAmount(amount + deliveryPrice)
    }

    fun updateConfirmationButtons() {
        val fragment = view as CartFragment
        val auth = fragment.activity.auth
        val userLoggedIn = auth.currentUser != null
        if (userLoggedIn) {
            view.displayConfirmOrderButton(true, view.getString(R.string.confirm_order))
            view.displayConfirmOrderWithoutLoginButton(false)
        }
        else {
            view.displayConfirmOrderButton(true, view.getString(R.string.login_and_checkout))
            view.displayConfirmOrderWithoutLoginButton(true, view.getString(R.string.checkout_without_login))
        }
    }

    interface View {
        fun displayDeliveryPrice(price: Double)
        fun displayAmount(amount: Double)
        fun displayConfirmOrderButton(display: Boolean, text: String = "")
        fun displayConfirmOrderWithoutLoginButton(display: Boolean, text: String = "")
    }
}