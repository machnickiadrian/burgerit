package com.szymanczyk.machnicki.burgerit.checkout

import com.szymanczyk.machnicki.burgerit.cart.CartRecord

class Order(
    var date: String,
    var client: Client,
    var products: List<CartRecord>,
    var amount: Double,
    var delivery: Delivery?,
    var location: String?
)

class Client(
    var name: String,
    var email: String,
    var phoneNumber: String
)

class Delivery(
    val price: Double,
    val name: String,
    val street: String,
    val city: String
)