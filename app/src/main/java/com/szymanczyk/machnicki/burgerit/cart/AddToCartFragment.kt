package com.szymanczyk.machnicki.burgerit.cart

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.menu.BURGERS
import com.szymanczyk.machnicki.burgerit.menu.MenuFragment
import com.szymanczyk.machnicki.burgerit.product.Product
import kotlinx.android.synthetic.main.fragment_add_to_cart.*
import kotlinx.android.synthetic.main.menu_item.*

const val LAST_MENU = "lastMenu"

class AddToCartFragment : Fragment(), AddToCartFragmentPresenter.View {

    private lateinit var activity: MainActivity
    private lateinit var presenter: AddToCartFragmentPresenter

    private lateinit var lastMenu: String
    private var snackSuggestions = mutableListOf<SnackSuggestionView>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            lastMenu = it.getString(LAST_MENU)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        activity.updateViewTitle(getString(R.string.title_add_to_cart))
        presenter = AddToCartFragmentPresenter(this)
        return inflater.inflate(R.layout.fragment_add_to_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.displayProductData(AppState.productToAddToCart)
        if (lastMenu == BURGERS) {
            presenter.displaySnacksSuggestions()
        }

        btn_add_to_cart.setOnClickListener {
            presenter.addProductsToCard(AppState.productToAddToCart, snackSuggestions)
        }
    }

    override fun displayProductName(name: String) {
        menu_item_title.text = name
    }

    override fun displayProductDetails(details: String) {
        menu_item_details.text = details
    }

    override fun displayProductPrice(price: Double) {
        menu_item_price.text = "%.2f".format(price)
    }

    override fun displaySnackSuggestion(snack: Product) {
        val snackSuggestion = SnackSuggestionView(context!!, snack)
        snackSuggestions.add(snackSuggestion)
        layout_add_to_cart_snacks?.addView(snackSuggestion)
    }

    override fun showProgressBar() {
        activity.showProgressBar()
    }

    override fun hideProgressBar() {
        activity.hideProgressBar()
    }

    override fun proceedToMenuScreen() {
        val fragment = MenuFragment.newInstance(lastMenu)
        activity.replaceFragment(fragment, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(lastMenu: String) =
            AddToCartFragment().apply {
                arguments = Bundle().apply {
                    putString(LAST_MENU, lastMenu)
                }
            }
    }
}
