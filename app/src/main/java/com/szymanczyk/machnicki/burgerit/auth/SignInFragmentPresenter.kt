package com.szymanczyk.machnicki.burgerit.auth

import android.util.Log
import com.google.firebase.auth.FirebaseAuth

class SignInFragmentPresenter(val view: View) {

    private var auth = FirebaseAuth.getInstance()

    fun attemptLogin(email: String, password: String) {
        if (isAnyLoginDataEmpty(email, password)) {
            return
        }

        view.showProgressBar()
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener((view as SignInFragment).activity!!) {
                view.hideProgressBar()
                if (it.isSuccessful) {
                    view.proceedToHomeScreen()
                } else {
                    Log.d(this::class.java.name, it.exception.toString())
                    val error = it.exception?.message ?: ""

                    if (error.contains("The email address is badly formatted.")) {
                        view.displayEmailErrorMessage("The email address is badly formatted.")
                    }

                    if (error.contains("There is no user record corresponding to this identifier")) {
                        view.displayEmailErrorMessage("There is no user with that email")
                    }

                    if (error.contains("The password is invalid")) {
                        view.displayPasswordErrorMessage("The password is invalid")
                    }
                }
            }
    }

    private fun isAnyLoginDataEmpty(email: String, password: String): Boolean {
        var anyEmpty = false

        if (email.isBlank()) {
            view.displayEmailErrorMessage("Email cannot be empty.")
            anyEmpty = true
        }

        if (password.isBlank()) {
            view.displayPasswordErrorMessage("Password cannot be blank")
            anyEmpty = true
        }
        return anyEmpty
    }

    interface View {
        fun displayEmailErrorMessage(message: String)
        fun displayPasswordErrorMessage(message: String)
        fun proceedToHomeScreen()
        fun showProgressBar()
        fun hideProgressBar()
    }

}