package com.szymanczyk.machnicki.burgerit.auth

class User() {
    var name: String? = null
    var email: String? = null
    var phoneNumber: String? = null

    constructor(name: String, email: String, phoneNumber: String): this() {
        this.name = name
        this.email = email
        this.phoneNumber = phoneNumber
    }

}