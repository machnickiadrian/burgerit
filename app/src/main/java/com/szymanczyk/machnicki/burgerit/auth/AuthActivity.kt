package com.szymanczyk.machnicki.burgerit.auth

import android.os.Bundle
import com.szymanczyk.machnicki.burgerit.BaseActivity
import com.szymanczyk.machnicki.burgerit.R

class AuthActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        val fragment = SignInFragment.newInstance()
        replaceFragment(fragment, false)
    }
}
