package com.szymanczyk.machnicki.burgerit.auth

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.szymanczyk.machnicki.burgerit.R
import kotlinx.android.synthetic.main.fragment_sign_in.*


class SignInFragment : Fragment(), SignInFragmentPresenter.View {

    private lateinit var activity: AuthActivity
    private lateinit var presenter: SignInFragmentPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as AuthActivity
        presenter = SignInFragmentPresenter(this)
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnSignIn.setOnClickListener(signInListener())
        btnSignUp.setOnClickListener(signUpListener())
    }

    private fun signInListener(): View.OnClickListener {
        return View.OnClickListener {
            val email = edtEmail.text.toString()
            val password = edtPassword.text.toString()
            presenter.attemptLogin(email, password)
        }
    }

    private fun signUpListener(): View.OnClickListener {
        return View.OnClickListener {
            val fragment = SignUpFragment.newInstance(edtEmail.text.toString())
            activity.replaceFragment(fragment)
        }
    }

    override fun displayEmailErrorMessage(message: String) {
        edtEmail.error = message
    }

    override fun displayPasswordErrorMessage(message: String) {
        edtPassword.error = message
    }

    override fun proceedToHomeScreen() {
        activity.setResult(Activity.RESULT_OK)
        activity.finish()
    }

    override fun showProgressBar() {
        activity.showProgressBar()
    }

    override fun hideProgressBar() {
        activity.hideProgressBar()
    }

    companion object {

        @JvmStatic
        fun newInstance() = SignInFragment()
    }
}
