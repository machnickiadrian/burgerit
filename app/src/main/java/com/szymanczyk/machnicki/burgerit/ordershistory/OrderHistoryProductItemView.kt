package com.szymanczyk.machnicki.burgerit.ordershistory

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.cart.CartRecord
import kotlinx.android.synthetic.main.layout_order_history_product_item.view.*

class OrderHistoryProductItemView(context: Context, val cartRecord: CartRecord): LinearLayout(context) {

    init {
        View.inflate(getContext(), R.layout.layout_order_history_product_item, this)
        tv_product_name.text = cartRecord.product.name
        tv_product_price.text = "${"%.2f".format(cartRecord.getPrice())} zł"
        tv_product_quantity.text = "${cartRecord.quantity}×"
    }

}