package com.szymanczyk.machnicki.burgerit.ordershistory


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.checkout.Order
import kotlinx.android.synthetic.main.fragment_orders_history.*

class OrdersHistoryFragment : Fragment(), OrdersHistoryFragmentPresenter.View {

    lateinit var activity: MainActivity
    lateinit var presenter: OrdersHistoryFragmentPresenter

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var orderHistoryRecordAdapter: OrderHistoryRecordAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        presenter = OrdersHistoryFragmentPresenter(this)
        activity.updateViewTitle(getString(R.string.title_orders_history))

        val view = inflater.inflate(R.layout.fragment_orders_history, container, false)
        recyclerView = view.findViewById(R.id.rv_order_list)

        val currentUser = activity.auth.currentUser
        if (currentUser != null) {
            presenter.fetchOrdersHistory(currentUser.email!!)
        }

        linearLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.setHasFixedSize(true)
        return view
    }

    override fun displayOrdersHistory(ordersList: MutableList<Order>) {
        orderHistoryRecordAdapter = OrderHistoryRecordAdapter(ordersList, this)
        recyclerView.adapter = orderHistoryRecordAdapter
    }

    override fun displayEmptyOrderHistoryInfo() {
        recyclerView.visibility = View.GONE
        tv_empty_orders_history.visibility = View.VISIBLE
    }

    override fun showProgressBar() {
        activity.showProgressBar()
    }

    override fun hideProgressBar() {
        activity.hideProgressBar()
    }

    companion object {
        @JvmStatic
        fun newInstance() = OrdersHistoryFragment()
    }
}
