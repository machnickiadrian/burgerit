package com.szymanczyk.machnicki.burgerit.aboutus.model

data class Location(
    val id: String,
    val name: String,
    val city: String,
    val address: String,
    val phoneNumber: String,
    val latitude: Double,
    val longitude: Double
)