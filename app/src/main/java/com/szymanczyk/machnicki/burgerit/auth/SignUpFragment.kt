package com.szymanczyk.machnicki.burgerit.auth

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.szymanczyk.machnicki.burgerit.R
import kotlinx.android.synthetic.main.fragment_sign_up.*

private const val PHONE_NUMBER = "phoneNumber"

class SignUpFragment : Fragment(), SignUpFragmentPresenter.View {

    private lateinit var activity: AuthActivity
    private lateinit var presenter: SignUpFragmentPresenter

    private lateinit var userEmail: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userEmail = it.getString(PHONE_NUMBER)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as AuthActivity
        presenter = SignUpFragmentPresenter(this)
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        edtEmail.setText(userEmail, TextView.BufferType.EDITABLE)
        btnSignUp.setOnClickListener(signUpListener())
    }

    private fun signUpListener(): View.OnClickListener {
        return View.OnClickListener {
            val email = edtEmail.text.toString()
            val name = edtName.text.toString()
            val password = edtPassword.text.toString()
            presenter.attemptSignUp(email, name, password)
        }
    }

    override fun displayEmailErrorMessage(message: String) {
        edtEmail.error = message
    }

    override fun displayNameErrorMessage(message: String) {
        edtName.error = message
    }

    override fun displayPasswordErrorMessage(message: String) {
        edtPassword.error = message
    }

    override fun proceedToHomeScreen() {
        activity.setResult(Activity.RESULT_OK)
        Toast.makeText(activity, "Thank you for registration ${activity.auth.currentUser?.email}", Toast.LENGTH_LONG).show()
        activity.finish()
    }

    companion object {

        @JvmStatic
        fun newInstance(phoneNumber: String) =
            SignUpFragment().apply {
                arguments = Bundle().apply {
                    putString(PHONE_NUMBER, phoneNumber)
                }
            }
    }
}
