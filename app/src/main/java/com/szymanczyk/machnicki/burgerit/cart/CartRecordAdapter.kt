package com.szymanczyk.machnicki.burgerit.cart

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.szymanczyk.machnicki.burgerit.R

class CartRecordAdapter(private var cartRecordList: MutableList<CartRecord>, private var view: CartFragment): RecyclerView.Adapter<CartRecordViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartRecordViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.cart_item, parent, false)
        return CartRecordViewHolder(view)
    }

    override fun getItemCount() = cartRecordList.size

    override fun onBindViewHolder(holder: CartRecordViewHolder, position: Int) {
        holder.tvProductName.text = cartRecordList[position].product.name
        holder.tvProductDetails.text = cartRecordList[position].product.details
        holder.tvProductAmount.text = "${cartRecordList[position].quantity}×"
        holder.tvProductPrice.text = "${"%.2f".format(cartRecordList[position].getPrice())} zł"

        holder.btnIncreaseAmount.setOnClickListener {
            cartRecordList[position].increaseQuantity()
            Log.d("Cart", Cart.products.toString())
            holder.tvProductAmount.text = "${cartRecordList[position].quantity}×"
            holder.tvProductPrice.text = "${"%.2f".format(cartRecordList[position].getPrice())} zł"
            view.presenter.updateCart()
        }
        holder.btnDecreaseAmout.setOnClickListener {
            if (cartRecordList[position].quantity == 1) {
                Cart.removeFromCart(cartRecordList[position].product)
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, cartRecordList.size)
            }
            else {
                cartRecordList[position].decreaseQuantity()
                holder.tvProductAmount.text = "${cartRecordList[position].quantity}×"
                holder.tvProductPrice.text = "${"%.2f".format(cartRecordList[position].getPrice())} zł"
            }

            Log.d("Cart", Cart.products.toString())
            view.presenter.updateCart()
        }

    }

}

class CartRecordViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var tvProductName: TextView = itemView.findViewById(R.id.tv_product_name)
    var tvProductDetails: TextView = itemView.findViewById(R.id.tv_product_details)
    var tvProductAmount: TextView = itemView.findViewById(R.id.tv_product_amount)
    var tvProductPrice: TextView = itemView.findViewById(R.id.tv_product_price)
    var btnIncreaseAmount: ImageButton = itemView.findViewById(R.id.btn_increase_amount)
    var btnDecreaseAmout: ImageButton = itemView.findViewById(R.id.btn_decrease_amount)
}