package com.szymanczyk.machnicki.burgerit.auth

import android.util.Log
import com.google.firebase.auth.FirebaseAuth

class SignUpFragmentPresenter(val view: View) {

    private var auth = FirebaseAuth.getInstance()

    fun attemptSignUp(email: String, name: String, password: String) {
        if (isAnySignUpDataEmpty(email, name, password)) {
            return
        }

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener((view as SignUpFragment).activity!!) {
                if (it.isSuccessful) {
                    view.proceedToHomeScreen()
                } else {
                    Log.d(this::class.java.name, it.exception.toString())
                    val error = it.exception?.message ?: ""

                    if (error == "The email address is badly formatted.") {
                        view.displayEmailErrorMessage("The email address is badly formatted.")
                    }

                    if (error == "The email address is already in use by another account.") {
                        view.displayEmailErrorMessage("The email address is already in use by another account")
                    }
                    if (error.contains("The given password is invalid")) {
                        view.displayPasswordErrorMessage("Password should be at least 6 characters long")
                    }

                }
            }
    }

    private fun isAnySignUpDataEmpty(email: String, name: String, password: String): Boolean {
        var anyEmpty = false

        if (email.isBlank()) {
            view.displayEmailErrorMessage("Email cannot be empty.")
            anyEmpty = true
        }

        if (name.isBlank()) {
            view.displayNameErrorMessage("Name cannot be empty")
            anyEmpty = true
        }

        if (password.isBlank()) {
            view.displayPasswordErrorMessage("Password cannot be blank")
            anyEmpty = true
        }
        return anyEmpty
    }

    interface View {
        fun displayEmailErrorMessage(message: String)
        fun displayNameErrorMessage(message: String)
        fun displayPasswordErrorMessage(message: String)
        fun proceedToHomeScreen()
    }
}