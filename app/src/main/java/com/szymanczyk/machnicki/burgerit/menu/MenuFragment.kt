package com.szymanczyk.machnicki.burgerit.menu

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.BaseFragment
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location
import com.szymanczyk.machnicki.burgerit.menu.spinner.SpinnerItemAdapter
import com.szymanczyk.machnicki.burgerit.menu.spinner.SpinnerItemState
import com.szymanczyk.machnicki.burgerit.product.Burger
import com.szymanczyk.machnicki.burgerit.product.Product
import kotlinx.android.synthetic.main.fragment_menu.*

const val MENU_TYPE = "menu"
const val BURGERS = "burgers"
const val SNACKS = "snacks"
const val DRINKS = "drinks"

class MenuFragment : BaseFragment() {

    private lateinit var activity: MainActivity

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var productAdapter: ProductAdapter

    private lateinit var menuType: String

    private val products = mutableListOf<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            menuType = it.getString(MENU_TYPE)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        activity.updateViewTitle(getString(R.string.title_menu))

        val view = inflater.inflate(R.layout.fragment_menu, container, false)
        if (menuType == BURGERS) {
            view.findViewById<View>(R.id.layout_menu_spinners).visibility = View.VISIBLE
        }
        fetchProducts(view)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // sort
        val sortItems = listOf(
            SpinnerItemState(getString(R.string.sort_by), getString(R.string.sort_by), false, null),
            SpinnerItemState(getString(R.string.price_by), getString(R.string.price_by), false,
                CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                    productAdapter.sortBy(ProductSortType.BY_PRICE_ASCENDING, isChecked)
                })
        )
        val sortAdapter = SpinnerItemAdapter(activity, R.id.spinner_sort, sortItems)
        spinner_sort.adapter = sortAdapter

        // filter
        val filterItems = listOf(
            SpinnerItemState(getString(R.string.filter), getString(R.string.filter), false, null),
            SpinnerItemState(getString(R.string.spicy_by), getString(R.string.spicy_by), false,
                CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                    productAdapter.filterBy(ProductFilterType.SPICY, isChecked)
                }
            ),
            SpinnerItemState(getString(R.string.vege_by), getString(R.string.vege_by), false,
                CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                    productAdapter.filterBy(ProductFilterType.VEGE, isChecked)
                }
            )
        )
        val filterAdapter = SpinnerItemAdapter(activity, R.id.spinner_filter, filterItems)
        spinner_filter.adapter = filterAdapter
    }

    private fun fetchProducts(view: View) {
        FirebaseDatabase.getInstance().reference.child(menuType)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach {
                        val product = when (menuType) {
                            BURGERS -> Burger(
                                id = it.key.toString(),
                                name = it.child("name").value.toString(),
                                price = it.child("price").value.toString().toDouble(),
                                elements = it.child("elements").children.map { it.value.toString() },
                                spicy = it.child("spicy").value.toString().toBoolean(),
                                vege = it.child("vege").value.toString().toBoolean(),
                                locationName = it.child("locationName").value.toString()
                            )

                            else -> Product(
                                id = it.key.toString(),
                                name = it.child("name").value.toString(),
                                price = it.child("price").value.toString().toDouble(),
                                details = it.child("details").value.toString()
                            )
                        }

                        if (product is Burger) {
                            val selectedLocation = AppState.getSelectedLocation()
                            if (selectedLocation == null && (product.locationName == null || product.locationName == "null")) {
                                products.add(product)
                            }
                            if (selectedLocation != null && (product.locationName == selectedLocation.name || product.locationName == null || product.locationName == "null")) {
                                products.add(product)
                            }

                        } else {
                            products.add(product)
                        }

                    }

                    recyclerView = view.findViewById(R.id.menu_list)
                    productAdapter = ProductAdapter(products, this@MenuFragment, menuType)
                    linearLayoutManager = LinearLayoutManager(activity)
                    recyclerView.layoutManager = linearLayoutManager
                    recyclerView.setHasFixedSize(true)
                    recyclerView.adapter = productAdapter
                }

                override fun onCancelled(error: DatabaseError) {
                    // do nothing
                }

            })
    }

    override fun onLocationSelected(location: Location, locationIndex: Int) {
        val fragment = MenuFragment.newInstance(menuType)
        activity.replaceFragment(fragment, false)
    }


    companion object {

        @JvmStatic
        fun newInstance(menuType: String) =
            MenuFragment().apply {
                arguments = Bundle().apply {
                    putString(MENU_TYPE, menuType)
                }
            }
    }
}

class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var root: LinearLayout = itemView.findViewById(R.id.list_root)
    var tvTitle: TextView = itemView.findViewById(R.id.menu_item_title)
    var tvDetails: TextView = itemView.findViewById(R.id.menu_item_details)
    var tvPrice: TextView = itemView.findViewById(R.id.menu_item_price)
    var ivSpicyMark: ImageView = itemView.findViewById(R.id.menu_item_spicy_mark)
    var ivVegeMark: ImageView = itemView.findViewById(R.id.menu_item_vege_mark)

}