package com.szymanczyk.machnicki.burgerit

import com.szymanczyk.machnicki.burgerit.aboutus.model.Location
import com.szymanczyk.machnicki.burgerit.cart.Cart
import com.szymanczyk.machnicki.burgerit.product.Product

object AppState {
    lateinit var productToAddToCart: Product
    private var selectedLocation: Location? = null
    private var selectedLocationIndex: Int = -1

    fun selectLocation(location: Location, locationIndex: Int) {
        selectedLocation = location
        selectedLocationIndex = locationIndex
        Cart.onLocationChange(location)
    }

    fun getSelectedLocation() = selectedLocation
    fun getSelectedLocationIndex() = selectedLocationIndex
}