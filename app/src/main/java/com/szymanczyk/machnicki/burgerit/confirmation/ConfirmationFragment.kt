package com.szymanczyk.machnicki.burgerit.confirmation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.cart.Cart
import com.szymanczyk.machnicki.burgerit.menu.BURGERS
import com.szymanczyk.machnicki.burgerit.menu.MenuFragment
import kotlinx.android.synthetic.main.fragment_confirmation.*

private const val ORDER_ID = "orderId"

class ConfirmationFragment : Fragment() {

    private lateinit var activity: MainActivity
    private lateinit var orderId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MainActivity
        arguments?.let {
            orderId = it.getString(ORDER_ID)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity.updateViewTitle(getString(R.string.title_order_confirmation))
        return inflater.inflate(R.layout.fragment_confirmation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tv_order_number.text = orderId
        tv_waiting_time.text = "30 min"
        btn_ok.setOnClickListener {
            val fragment = MenuFragment.newInstance(BURGERS)
            activity.supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            activity.replaceFragment(fragment, false)
            Cart.clearCart()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(orderId: String) =
            ConfirmationFragment().apply {
                arguments = Bundle().apply {
                    putString(ORDER_ID, orderId)
                }
            }
    }
}
