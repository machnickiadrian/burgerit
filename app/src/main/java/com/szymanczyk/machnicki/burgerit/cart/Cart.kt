package com.szymanczyk.machnicki.burgerit.cart

import android.util.Log
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location
import com.szymanczyk.machnicki.burgerit.product.Burger
import com.szymanczyk.machnicki.burgerit.product.Product

object Cart {

    lateinit var activity: MainActivity
    private var deliveryRequired = false
    private var deliveryPrice = 0.0

    val products = mutableListOf<CartRecord>()

    fun addDelivery(deliveryPrice: Double) {
        deliveryRequired = true
        this.deliveryPrice = deliveryPrice
    }

    fun clearDelivery() {
        deliveryRequired = false
        this.deliveryPrice = 0.0
    }

    fun getDeliveryPrice() = deliveryPrice

    fun isEmpty() = products.isEmpty()

    fun getAmount() = products.map { it.getPrice() }.sum()

    fun getProductsNumber() = products.map { it.quantity }.sum()

    fun clearCart() {
        products.clear()
        activity.invalidateOptionsMenu()
    }

    fun addToCart(product: Product, quantity: Int = 1) {
        if (!containsProduct(product)) {
            Log.d("Cart", "Didn't contain")
            products.add(CartRecord(product, quantity))
            activity.invalidateOptionsMenu()
            return
        }

        Log.d("Cart", "Did contain")

        increaseQuantity(product, quantity)
        activity.invalidateOptionsMenu()
    }

    fun decreaseQuantity(product: Product, quantity: Int = 1) {
        increaseQuantity(product, -quantity)
    }

    fun removeFromCart(product: Product) {
        products.removeIf {
            it.product.name == product.name
        }
        Log.d("Cart", "${product.name} removed")
        activity.invalidateOptionsMenu()
    }

    private fun containsProduct(product: Product) = products.any { it.product.name == product.name }

    private fun increaseQuantity(product: Product, quantity: Int) {
        val foundProduct = products.find { it.product.name == product.name }
        foundProduct?.quantity = quantity + foundProduct?.quantity!!
        activity.invalidateOptionsMenu()
    }

    fun onLocationChange(location: Location) {
        products.removeIf {
            if (it.product is Burger) {
                if (it.product.locationName == null || it.product.locationName == "null") {
                    false
                } else it.product.locationName != location.name
            } else {
                false
            }

        }

        activity.invalidateOptionsMenu()
    }

}