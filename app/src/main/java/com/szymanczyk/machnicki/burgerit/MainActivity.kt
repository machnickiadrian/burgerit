package com.szymanczyk.machnicki.burgerit

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.szymanczyk.machnicki.burgerit.aboutus.AboutUsFragment
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location
import com.szymanczyk.machnicki.burgerit.auth.AuthActivity
import com.szymanczyk.machnicki.burgerit.auth.SignInFragment
import com.szymanczyk.machnicki.burgerit.cart.Cart
import com.szymanczyk.machnicki.burgerit.cart.CartFragment
import com.szymanczyk.machnicki.burgerit.home.HomeFragment
import com.szymanczyk.machnicki.burgerit.menu.BURGERS
import com.szymanczyk.machnicki.burgerit.menu.DRINKS
import com.szymanczyk.machnicki.burgerit.menu.MenuFragment
import com.szymanczyk.machnicki.burgerit.menu.SNACKS
import com.szymanczyk.machnicki.burgerit.ordershistory.OrdersHistoryFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

const val AUTH_REQUEST = 1

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, MainActivityPresenter.View {

    private lateinit var presenter: MainActivityPresenter
    private lateinit var menu: Menu
    private lateinit var tvCartItemCount: TextView
    private var fragment: BaseFragment? = null

    private var locations = mutableListOf<Location>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        presenter = MainActivityPresenter(this)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.nav_drawer_open, R.string.nav_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        menu = navigation_view.menu
        navigation_view.setNavigationItemSelectedListener(this)

        val fragment = HomeFragment.newInstance()
        replaceFragment(fragment, false)

        presenter.fetchLocations(true)

        val userLoggedIn = auth.currentUser != null
        refreshUsernameOnHeader(userLoggedIn)
    }

    override fun onResume() {
        super.onResume()
        val userLoggedIn = auth.currentUser != null
        refreshMenuItems(userLoggedIn)
        refreshUsernameOnHeader(userLoggedIn)

        Cart.activity = this

        invalidateOptionsMenu()
    }

    private fun refreshUsernameOnHeader(userLoggedIn: Boolean) {
        if (userLoggedIn) {
            val username = auth.currentUser?.email ?: ""
            setHeaderTexts(username, "")
        } else {
            setHeaderTexts(getString(R.string.nav_header_guest), "")
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        val cartItem = menu.findItem(R.id.action_cart)
        val actionView = cartItem.actionView
        tvCartItemCount = actionView.findViewById(R.id.cart_badge)
        actionView.setOnClickListener { onOptionsItemSelected(cartItem) }

        val actionIcon = actionView.findViewById<ImageView>(R.id.cart_icon)

        val enableCartAction = !Cart.isEmpty()
        actionView.isEnabled = enableCartAction
        cartItem.isEnabled = enableCartAction
        when (cartItem.isEnabled) {
            true -> actionIcon.setImageResource(R.drawable.ic_shopping_cart_white_24dp)
            false -> actionIcon.setImageResource(R.drawable.ic_shopping_cart_grey_24dp)
        }
        setupBadge()

        return true
    }


    private fun setupBadge() {
        if (Cart.isEmpty()) {
            tvCartItemCount.visibility = View.GONE
        } else {
            tvCartItemCount.text = Cart.getProductsNumber().toString()
            tvCartItemCount.visibility = View.VISIBLE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_cart) {
            if (Cart.isEmpty()) {
                return false
            }

            val fragment = CartFragment.newInstance()
            replaceFragment(fragment)
            return true
        }

        if (item.itemId == R.id.action_location) {
            presenter.fetchLocations(false)
            return true
        }

        super.onOptionsItemSelected(item)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.nav_log_in) {
            val authIntent = Intent(this, AuthActivity::class.java)
            startActivityForResult(authIntent, AUTH_REQUEST)
            drawer_layout.closeDrawer(GravityCompat.START)
            return true
        }

        val fragment = when (item.itemId) {
            R.id.nav_burgers -> MenuFragment.newInstance(BURGERS)
            R.id.nav_snacks -> MenuFragment.newInstance(SNACKS)
            R.id.nav_drinks -> MenuFragment.newInstance(DRINKS)
            R.id.nav_log_in -> SignInFragment.newInstance()
            R.id.nav_about_us -> AboutUsFragment.newInstance()
            R.id.nav_orders -> OrdersHistoryFragment.newInstance()
            R.id.nav_log_out -> {
                auth.signOut()
                drawer_layout.closeDrawer(GravityCompat.START)
                refreshMenuItems(false)
                setHeaderTexts(getString(R.string.nav_header_guest), "")
                HomeFragment.newInstance()
            }
            else -> {
                drawer_layout.closeDrawer(GravityCompat.START)
                return true
            }
        }

        replaceFragment(fragment)

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTH_REQUEST && resultCode == Activity.RESULT_OK) {
            val username = auth.currentUser?.email ?: ""
            setHeaderTexts(username, "")
            refreshMenuItems(true)

            Toast.makeText(this, "Hello $username", Toast.LENGTH_SHORT).show()
        }
    }

    override fun replaceFragment(fragment: Fragment, addToBackStack: Boolean) {
        super.replaceFragment(fragment, addToBackStack)
        if (fragment is BaseFragment) {
            this.fragment = fragment
        }
        else {
            this.fragment = null
        }
    }

    override fun onLocationSelected(location: Location, locationIndex: Int) {
        fragment?.onLocationSelected(location, locationIndex)
    }

    private fun refreshMenuItems(userLoggedIn: Boolean) {
        menu.findItem(R.id.nav_log_in).isVisible = !userLoggedIn
        menu.findItem(R.id.nav_log_out).isVisible = userLoggedIn
        menu.findItem(R.id.nav_orders).isVisible = userLoggedIn
    }


    private fun setHeaderTexts(username: String, userCity: String) {
        val navigationView: NavigationView = findViewById(R.id.navigation_view)
        val header = navigationView.getHeaderView(0)
        header.findViewById<TextView>(R.id.userName)?.text = username
        header.findViewById<TextView>(R.id.userCity)?.text = userCity
    }

    override fun updateViewTitle(title: String) {
        toolbar.title = title
    }

    override fun updateUsername(username: String) {
        findViewById<TextView>(R.id.userName)?.text = username
    }

    override fun displayMenuItem(itemId: Int, display: Boolean) {
        menu.findItem(itemId)?.isVisible = display
    }

    override fun addLocation(location: Location) {
        if(!locations.contains(location)) {
            locations.add(location)
        }
    }

    override fun displayLocationsDialogOnAppStart() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val selectedLocation = sharedPref.getString(getString(R.string.preference_location), null)

        if (selectedLocation == null) {
            displayLocationsDialog()
        }
        else {
            val locationIndex = locations.indexOfFirst { selectedLocation == it.name }
            val location = locations[locationIndex]
            presenter.selectLocation(location, locationIndex)
        }
    }

    override fun displayLocationsDialog() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(getString(R.string.select_location))

        dialogBuilder
            .setSingleChoiceItems(locations.map { it.name }.toTypedArray(), AppState.getSelectedLocationIndex()) {
                    dialog, which ->
                presenter.selectLocation(locations[which], which)
                with(sharedPref.edit()) {
                    putString(getString(com.szymanczyk.machnicki.burgerit.R.string.preference_location), locations[which].name)
                    apply()
                }
                dialog.dismiss()
            }

        val alertDialog = dialogBuilder.create()
        alertDialog.show()
    }


}
