package com.szymanczyk.machnicki.burgerit.cart

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.szymanczyk.machnicki.burgerit.menu.SNACKS
import com.szymanczyk.machnicki.burgerit.product.Product

class AddToCartFragmentPresenter(val view: View) {

    private val database = FirebaseDatabase.getInstance().reference

    fun addProductsToCard(product: Product, snackSuggestions: MutableList<SnackSuggestionView>) {
        Cart.addToCart(product)
        snackSuggestions.filter { it.isChecked() }
            .forEach { Cart.addToCart(it.product) }

        view.proceedToMenuScreen()
    }

    fun displayProductData(product: Product) {
        view.displayProductName(product.name)
        view.displayProductDetails(product.details)
        view.displayProductPrice(product.price)
    }

    fun displaySnacksSuggestions() {
        view.showProgressBar()
        database.child(SNACKS).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children
                    .mapNotNull {
                        Product(
                            id = it.key.toString(),
                            name = it.child("name").value.toString(),
                            price = it.child("price").value.toString().toDouble(),
                            details = it.child("details").value.toString()
                        )
                    }
                    .forEach { view.displaySnackSuggestion(it) }

                view.hideProgressBar()
            }

            override fun onCancelled(p0: DatabaseError) {
                // do nothing
            }

        })
    }

    interface View {
        fun displayProductName(name: String)
        fun displayProductDetails(details: String)
        fun displayProductPrice(price: Double)
        fun displaySnackSuggestion(snack: Product)
        fun showProgressBar()
        fun hideProgressBar()
        fun proceedToMenuScreen()
    }
}