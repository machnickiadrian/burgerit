package com.szymanczyk.machnicki.burgerit.cart

import android.content.Context
import android.view.View
import android.widget.RelativeLayout
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.product.Product
import kotlinx.android.synthetic.main.layout_add_snack_to_cart.view.*

class SnackSuggestionView(context: Context, val product: Product) : RelativeLayout(context) {

    init {
        View.inflate(getContext(), R.layout.layout_add_snack_to_cart, this)
        tv_snack_description.text = "Dodaj ${product.name.toLowerCase()}"
        tv_snack_price.text = "%.2f".format(product.price)

        tv_snack_description.setOnClickListener {
            cb_snack.toggle()
        }
    }

    fun isChecked() = cb_snack.isChecked

}