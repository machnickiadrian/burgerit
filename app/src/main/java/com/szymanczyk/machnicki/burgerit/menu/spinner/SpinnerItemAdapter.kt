package com.szymanczyk.machnicki.burgerit.menu.spinner

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.szymanczyk.machnicki.burgerit.R

class SpinnerItemAdapter(private var mContext: Context, resource: Int, private val items: List<SpinnerItemState>) :
    ArrayAdapter<SpinnerItemState>(mContext, resource, items) {

    private var isFromView = false

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    private fun getCustomView(position: Int, convertView: View?, parent: ViewGroup): View {

        var convView = convertView

        val holder: SpinnerItemViewHolder = if (convView == null) {
            val layoutInflater = LayoutInflater.from(mContext)
            convView = layoutInflater.inflate(R.layout.spinner_item, null)
            val mHolder = SpinnerItemViewHolder(
                convView.findViewById(R.id.tv_spinner_item_title),
                convView.findViewById(R.id.cb_spinner_item)
            )
            convView.tag = mHolder
            mHolder
        } else {
            convView.tag as SpinnerItemViewHolder
        }

        holder.setTitle(items[position].title)
        isFromView = true
        holder.setChecked(items[position].selected)
        isFromView = false

        if (position == 0) {
            holder.cb.visibility = View.INVISIBLE
        } else {
            holder.cb.visibility = View.VISIBLE
        }
        holder.cb.tag = position
        holder.cb.setOnCheckedChangeListener { buttonView, isChecked ->
            val pos = buttonView.tag as Int
            if (!isFromView) {
                items[pos].selected = isChecked
                items[pos].listener?.onCheckedChanged(buttonView, isChecked)
            }
        }

        return convView!!
    }

}

private class SpinnerItemViewHolder(val tv: TextView, val cb: CheckBox) {

    fun setTitle(title: String) {
        tv.text = title
    }

    fun setChecked(checked: Boolean) {
        cb.isChecked = checked
    }

}