package com.szymanczyk.machnicki.burgerit.cart

import com.szymanczyk.machnicki.burgerit.product.Product

data class CartRecord(val product: Product, var quantity: Int = 1) {

    fun getPrice() = product.price * quantity

    fun increaseQuantity() {
        Cart.addToCart(product)
    }

    fun decreaseQuantity() {
        Cart.decreaseQuantity(product)
    }

}