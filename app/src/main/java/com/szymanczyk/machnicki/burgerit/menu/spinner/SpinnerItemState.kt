package com.szymanczyk.machnicki.burgerit.menu.spinner

import android.widget.CompoundButton

class SpinnerItemState(val name: String, val title: String, var selected: Boolean, var listener: CompoundButton.OnCheckedChangeListener?)