package com.szymanczyk.machnicki.burgerit.checkout

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.szymanczyk.machnicki.burgerit.MainActivity

import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.cart.Cart
import com.szymanczyk.machnicki.burgerit.confirmation.ConfirmationFragment
import kotlinx.android.synthetic.main.fragment_checkout_delivery_address.*

private const val NAME = "name"
private const val PHONE_NUMBER = "phone"
private const val EMAIL = "email"

class CheckoutDeliveryAddressFragment : Fragment(), CheckoutDeliveryAddressFragmentPresenter.View {

    private lateinit var activity: MainActivity
    private lateinit var presenter: CheckoutDeliveryAddressFragmentPresenter

    private lateinit var name: String
    private lateinit var phoneNumber: String
    private lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            name = it.getString(NAME)!!
            phoneNumber = it.getString(PHONE_NUMBER)!!
            email = it.getString(EMAIL)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        presenter = CheckoutDeliveryAddressFragmentPresenter(this)
        activity.updateViewTitle(getString(R.string.title_checkout))
        return inflater.inflate(R.layout.fragment_checkout_delivery_address, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tv_delivery_price.text = "${"%.2f".format(Cart.getDeliveryPrice())} zł"
        tv_amount.text = "${"%.2f".format(Cart.getAmount() + Cart.getDeliveryPrice())} zł"
        btn_confirm.setOnClickListener {
            val street = activity.findViewById<EditText>(R.id.edt_street).text.toString()
            val city = activity.findViewById<EditText>(R.id.edt_city).text.toString()
            presenter.confirmOrder(name, email, phoneNumber, street, city)
        }
    }

    override fun displayStreetErrorMessage(message: String) {
        edt_street.error = message
    }

    override fun displayCityErrorMessage(message: String) {
        edt_city.error = message
    }

    override fun showProgressBar() {
        activity.showProgressBar()
    }

    override fun hideProgressBar() {
        activity.hideProgressBar()
    }

    override fun proceedToConfirmationScreen(orderId: String) {
        val fragment = ConfirmationFragment.newInstance(orderId)
        activity.replaceFragment(fragment, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(name: String, phoneNumber: String, email: String) =
            CheckoutDeliveryAddressFragment().apply {
                arguments = Bundle().apply {
                    putString(NAME, name)
                    putString(PHONE_NUMBER, phoneNumber)
                    putString(EMAIL, email)
                }
            }
    }
}
