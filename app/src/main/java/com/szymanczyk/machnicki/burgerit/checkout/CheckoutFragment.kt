package com.szymanczyk.machnicki.burgerit.checkout

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.cart.Cart
import com.szymanczyk.machnicki.burgerit.confirmation.ConfirmationFragment
import kotlinx.android.synthetic.main.fragment_checkout.*

class CheckoutFragment : Fragment(), CheckoutFragmentPresenter.View {

    private lateinit var activity: MainActivity
    private lateinit var presenter: CheckoutFragmentPresenter
    private lateinit var database: DatabaseReference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        presenter = CheckoutFragmentPresenter(this)
        database = FirebaseDatabase.getInstance().reference
        activity.updateViewTitle(getString(R.string.title_checkout))
        return inflater.inflate(R.layout.fragment_checkout, container, false)
    }

    override fun onResume() {
        super.onResume()
        val currentUser = activity.auth.currentUser
        if (currentUser != null) {
            presenter.fillFormWithUserData(currentUser)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tv_delivery_price.text = "0,00 zł"
        tv_amount.text = "${"%.2f".format(Cart.getAmount())} zł"
        btn_confirm.setOnClickListener {
            val name = activity.findViewById<EditText>(R.id.edt_name).text.toString()
            val email = activity.findViewById<EditText>(R.id.edt_email).text.toString()
            val phone = activity.findViewById<EditText>(R.id.edt_phone).text.toString()
            presenter.confirmOrder(name, email, phone, radio_group_delivery.checkedRadioButtonId)
        }

        radio_group_delivery.setOnCheckedChangeListener { group, checkedId ->
            clearDeliveryMethodError()
            when(checkedId) {
                R.id.radio_item_take_away -> presenter.clearDelivery()
                R.id.radio_item_delivery -> presenter.addDelivery(11.0)
            }


        }
    }

    override fun displayUserData(name: String?, email: String?, phone: String?) {
        edt_name.setText(name ?: "")
        edt_email.setText(email ?: "")
        edt_phone.setText(phone ?: "")

        if (edt_email.text.toString() != "") {
            edt_email.isEnabled = false
        }
    }

    override fun displayNameErrorMessage(message: String) {
        edt_name.error = message
    }

    override fun displayEmailErrorMessage(message: String) {
        edt_email.error = message
    }

    override fun displayPhoneNumberErrorMessage(message: String) {
        edt_phone.error = message
    }

    override fun displayPrices(deliveryPrice: Double, amount: Double) {
        tv_delivery_price.text = "${"%.2f".format(deliveryPrice)} zł"
        tv_amount.text = "${"%.2f".format(amount)} zł"
    }

    override fun displayDeliveryMethodError() {
        radio_item_take_away.error = "ERROR"
        radio_item_delivery.error = "ERROR"
    }

    override fun clearDeliveryMethodError() {
        radio_item_take_away.error = null
        radio_item_delivery.error = null
    }

    override fun showProgressBar() {
        activity.showProgressBar()
    }

    override fun hideProgressBar() {
        activity.hideProgressBar()
    }

    override fun proceedToCheckoutDeliveryAddressScreen(name: String, phoneNumber: String, email: String) {
        val fragment = CheckoutDeliveryAddressFragment.newInstance(name, phoneNumber, email)
        activity.replaceFragment(fragment)
    }

    override fun proceedToConfirmationScreen(orderId: String) {
        val fragment = ConfirmationFragment.newInstance(orderId)
        activity.replaceFragment(fragment, false)
    }

    companion object {

        @JvmStatic
        fun newInstance() = CheckoutFragment()
    }
}
