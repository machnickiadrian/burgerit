package com.szymanczyk.machnicki.burgerit.aboutus

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.BaseFragment
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location
import kotlinx.android.synthetic.main.fragment_about_us.*

class AboutUsFragment : BaseFragment(), OnMapReadyCallback, AboutUsFragmentPresenter.View {

    private lateinit var activity: MainActivity
    private lateinit var presenter: AboutUsFragmentPresenter
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var mMap: GoogleMap

    private var locations = mutableListOf<Location>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        presenter = AboutUsFragmentPresenter(this)
        val view = inflater.inflate(R.layout.fragment_about_us, container, false)
        mapFragment = SupportMapFragment.newInstance()
        mapFragment.getMapAsync(this)

        childFragmentManager.beginTransaction()
            .replace(R.id.fragment_map_container, mapFragment)
            .commit()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btn_select_location.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(activity)
            dialogBuilder.setTitle(getString(R.string.select_location))

            dialogBuilder.setSingleChoiceItems(
                locations.map { it.name }.toTypedArray(),
                AppState.getSelectedLocationIndex()
            ) { dialog, which ->
                Log.d("Selected location", locations[which].name)
                presenter.selectLocation(locations[which], which)
                dialog.dismiss()
            }

            val alertDialog = dialogBuilder.create()
            alertDialog.show()
        }

        ib_call_us.setOnClickListener {
            val selectedLocation = AppState.getSelectedLocation()
            val phoneNumber = selectedLocation?.phoneNumber ?: tv_about_us_local_phone.text

            val callIntent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:$phoneNumber")
            startActivity(callIntent)
        }
    }

    override fun onResume() {
        super.onResume()
        val selectedLocation = AppState.getSelectedLocation()
        btn_select_location.text = selectedLocation?.name ?: getString(R.string.select_location)
        if (selectedLocation != null) {
            updateInfoFooter(selectedLocation)
        }
    }

    override fun onLocationSelected(location: Location, locationIndex: Int) {
        presenter.selectLocation(location, locationIndex)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(51.76, 19.46)))
        presenter.displayLocations()
        val selectedLocation = AppState.getSelectedLocation()
        if (selectedLocation != null) {
            moveMapToSelectedLocation(selectedLocation)
        }

    }

    override fun displayLocationOnMap(location: Location) {
        locations.add(location)
        val position = LatLng(location.latitude, location.longitude)
        mMap.addMarker(
            MarkerOptions()
                .position(position)
                .title(location.name)
        )

        if (locations.indexOf(location) == 0) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 5f))
        }
    }

    override fun updateSelectLocationButtonText(label: String) {
        btn_select_location.text = label
    }

    override fun updateInfoFooter(location: Location) {
        tv_about_us_local_name.text = location.name
        tv_about_us_local_address.text = location.address
        tv_about_us_local_city.text = location.city
        tv_about_us_local_phone.text = location.phoneNumber
    }

    override fun moveMapToSelectedLocation(location: Location) {
        mMap.animateCamera(
            CameraUpdateFactory.newLatLng(
                LatLng(location.latitude, location.longitude)
            )
        )
    }

    override fun showProgressBar() {
        activity.showProgressBar()
    }

    override fun hideProgressBar() {
        activity.hideProgressBar()
    }

    companion object {

        @JvmStatic
        fun newInstance() = AboutUsFragment()
    }
}
