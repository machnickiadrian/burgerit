package com.szymanczyk.machnicki.burgerit.checkout

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserInfo
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.auth.User
import com.szymanczyk.machnicki.burgerit.cart.Cart
import java.time.LocalDateTime

class CheckoutFragmentPresenter(val view: View) {

    private val database = FirebaseDatabase.getInstance().reference

    fun fillFormWithUserData(currentUser: UserInfo) {
        view.showProgressBar()
        database.child("users").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                view.hideProgressBar()
                val userSnapshot = snapshot.children.find { it.child("email").value == currentUser.email }
                if (userSnapshot != null) {
                    val user = userSnapshot.getValue(User::class.java)
                    if (user != null) {
                        view.displayUserData(user.name, user.email, user.phoneNumber)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // do nothing
            }
        })

    }

    fun confirmOrder(name: String, email: String, phoneNumber: String, deliveryMethod: Int) {
        if (isAnyDataEmpty(name, email, phoneNumber, deliveryMethod != -1)) {
            return
        }

        val order = Order(
            date = LocalDateTime.now().toString(),
            client = Client(name, email, phoneNumber),
            products = Cart.products.toList(),
            amount = Cart.getAmount(),
            delivery = null,
            location = AppState.getSelectedLocation()?.name ?: ""
        )

        if (deliveryMethod == R.id.radio_item_take_away) {
            view.showProgressBar()
            val orderKey = database.child("orders").push().key ?: return
            database.child("orders").child(orderKey).setValue(order)
            updateUserData(name, email, phoneNumber)
            view.hideProgressBar()
            view.proceedToConfirmationScreen(orderKey)
            return
        }

        // if delivery
        // proceed to screen with delivery address form
        updateUserData(name, email, phoneNumber)
        view.proceedToCheckoutDeliveryAddressScreen(name, phoneNumber, email)
    }

    private fun updateUserData(name: String, email: String, phone: String) {
        val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email
        if (currentUserEmail != null) {
            database.child("users").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val userSnapshot = snapshot.children.find { it.child("email").value == currentUserEmail }
                    val key = when (userSnapshot != null) {
                        true -> userSnapshot.key
                        false -> database.child("users").push().key
                    }

                    if (key != null) {
                        val user = User(name, email, phone)
                        database.child("users").child(key).setValue(user)
                    }

                    view.hideProgressBar()
                }

                override fun onCancelled(p0: DatabaseError) {
                    // do nothing
                }
            })

        }

    }

    private fun isAnyDataEmpty(name: String, email: String, phone: String, deliveryMethodChecked: Boolean): Boolean {
        var anyEmpty = false

        if (name.isBlank()) {
            view.displayNameErrorMessage("Name cannot be empty")
            anyEmpty = true
        }

        if (email.isBlank()) {
            view.displayEmailErrorMessage("Email cannot be empty")
            anyEmpty = true
        }

        if (phone.isBlank()) {
            view.displayPhoneNumberErrorMessage("Phone number cannot be empty")
            anyEmpty = true
        }

        if (!deliveryMethodChecked) {
            view.displayDeliveryMethodError()
            anyEmpty = true
        } else {
            view.clearDeliveryMethodError()
        }

        return anyEmpty
    }

    fun addDelivery(deliveryPrice: Double) {
        Cart.addDelivery(deliveryPrice)
        view.displayPrices(deliveryPrice, Cart.getAmount() + deliveryPrice)
    }

    fun clearDelivery() {
        Cart.clearDelivery()
        view.displayPrices(0.0, Cart.getAmount())
    }

    interface View {
        fun displayUserData(name: String?, email: String?, phone: String?)
        fun displayNameErrorMessage(message: String)
        fun displayEmailErrorMessage(message: String)
        fun displayPhoneNumberErrorMessage(message: String)
        fun displayPrices(deliveryPrice: Double, amount: Double)
        fun displayDeliveryMethodError()
        fun clearDeliveryMethodError()
        fun showProgressBar()
        fun hideProgressBar()
        fun proceedToCheckoutDeliveryAddressScreen(name: String, phoneNumber: String, email: String)
        fun proceedToConfirmationScreen(orderId: String)
    }
}