package com.szymanczyk.machnicki.burgerit.cart

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.szymanczyk.machnicki.burgerit.BaseFragment
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location
import com.szymanczyk.machnicki.burgerit.auth.AuthActivity
import com.szymanczyk.machnicki.burgerit.checkout.CheckoutFragment
import kotlinx.android.synthetic.main.fragment_cart.*

const val AUTH_REQUEST = 1

class CartFragment : BaseFragment(), CartFragmentPresenter.View {

    lateinit var activity: MainActivity
    lateinit var presenter: CartFragmentPresenter

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var cartRecordAdapter: CartRecordAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        presenter = CartFragmentPresenter(this)
        activity.updateViewTitle(getString(R.string.title_cart))

        val view = inflater.inflate(R.layout.fragment_cart, container, false)
        recyclerView = view.findViewById(R.id.cart_list)
        cartRecordAdapter = CartRecordAdapter(Cart.products, this)
        linearLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = cartRecordAdapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btn_confirm_order.setOnClickListener {
            if (activity.auth.currentUser == null) {
                val authIntent = Intent(activity, AuthActivity::class.java)
                startActivityForResult(authIntent, AUTH_REQUEST)
                return@setOnClickListener
            }

            val fragment = CheckoutFragment.newInstance()
            activity.replaceFragment(fragment)
        }

        btn_confirm_order_without_login.setOnClickListener {
            val fragment = CheckoutFragment.newInstance()
            activity.replaceFragment(fragment)
        }

    }

    override fun onResume() {
        super.onResume()
        presenter.updateCart()
        presenter.updateConfirmationButtons()
    }

    override fun onLocationSelected(location: Location, locationIndex: Int) {
        activity.replaceFragment(CartFragment.newInstance(), false)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("Activity result", "RESULT NOT OK")

        if (requestCode == AUTH_REQUEST && resultCode == Activity.RESULT_OK) {
            val fragment = CheckoutFragment.newInstance()
            activity.replaceFragment(fragment)
        }
    }

    override fun displayDeliveryPrice(price: Double) {
        tv_delivery_price.text = "${"%.2f".format(price)} zł"
    }

    override fun displayAmount(amount: Double) {
        tv_amount.text = "${"%.2f".format(amount)} zł"
    }

    override fun displayConfirmOrderButton(display: Boolean, text: String) {
        btn_confirm_order.text = text
        if (display) {
            btn_confirm_order.visibility = View.VISIBLE
        } else {
            btn_confirm_order.visibility = View.GONE
        }
    }

    override fun displayConfirmOrderWithoutLoginButton(display: Boolean, text: String) {
        btn_confirm_order_without_login.text = text
        btn_confirm_order_without_login.visibility = when(display) {
            true -> View.VISIBLE
            false -> View.GONE
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = CartFragment()
    }
}
