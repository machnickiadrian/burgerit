package com.szymanczyk.machnicki.burgerit

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location

class MainActivityPresenter(val view: View) {

    private val database = FirebaseDatabase.getInstance().reference

    fun fetchLocations(appStart: Boolean) {
        // TODO extract to one place
        view.showProgressBar()
        database.child("locations").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.mapNotNull {
                    Location(
                        id = it.key.toString(),
                        name = it.child("name").value.toString(),
                        city = it.child("city").value.toString(),
                        address = it.child("address").value.toString(),
                        phoneNumber = it.child("phoneNumber").value.toString(),
                        latitude = it.child("latitude").value.toString().toDouble(),
                        longitude = it.child("longitude").value.toString().toDouble()
                    )
                }
                    .forEach { view.addLocation(it) }

                if (appStart) {
                    view.displayLocationsDialogOnAppStart()
                } else {
                    view.displayLocationsDialog()
                }
                view.hideProgressBar()
            }

            override fun onCancelled(p0: DatabaseError) {
                // do nothing
            }

        })

    }

    fun selectLocation(location: Location, locationIndex: Int) {
        AppState.selectLocation(location, locationIndex)
        // TODO
        // update view
        // probably all fragments presenters should implement this
        // TODO do it on adding location button to toolbar
        view.onLocationSelected(location, locationIndex)
    }

    interface View {
        fun updateViewTitle(title: String)
        fun updateUsername(username: String)
        fun displayMenuItem(itemId: Int, display: Boolean)
        fun addLocation(location: Location)
        fun displayLocationsDialogOnAppStart()
        fun displayLocationsDialog()
        fun onLocationSelected(location: Location, locationIndex: Int)
        fun showProgressBar()
        fun hideProgressBar()
    }

}