package com.szymanczyk.machnicki.burgerit.product

class Burger(
    id: String,
    name: String,
    price: Double,
    val elements: List<String>,
    val spicy: Boolean,
    val vege: Boolean,
    val locationName: String?
) : Product(id, name, price, elements.joinToString()) {
    override fun toString(): String {
        return super.toString() +
                "Burger(elements=$elements, spicy=$spicy, vege=$vege, locationName=$locationName)"
    }
}