package com.szymanczyk.machnicki.burgerit

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.content_main.*

abstract class BaseActivity: AppCompatActivity() {

    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }

    open fun replaceFragment(fragment: Fragment, addToBackStack: Boolean = true) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        if (addToBackStack) {
            transaction.addToBackStack(null)
        }
        transaction.commit()
    }

    fun showProgressBar() {
        Thread(Runnable {
            runOnUiThread { progress_bar.visibility = View.VISIBLE }
        }).start()
    }

    fun hideProgressBar() {
        Thread(Runnable {
            runOnUiThread { progress_bar.visibility = View.GONE }
        }).start()
    }

}