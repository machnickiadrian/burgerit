package com.szymanczyk.machnicki.burgerit.checkout

import com.google.firebase.database.FirebaseDatabase
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.cart.Cart
import java.time.LocalDateTime

class CheckoutDeliveryAddressFragmentPresenter(val view: View) {

    private val database = FirebaseDatabase.getInstance().reference

    fun confirmOrder(name: String, email: String, phoneNumber: String, street: String, city: String) {
        if (isAnyDataEmpty(street, city)) {
            return
        }

        val order = Order(
            date = LocalDateTime.now().toString(),
            client = Client(name, email, phoneNumber),
            products = Cart.products.toList(),
            amount = Cart.getAmount() + Cart.getDeliveryPrice(),
            delivery = Delivery(Cart.getDeliveryPrice(), name, street, city),
            location = AppState.getSelectedLocation()?.name
        )

        view.showProgressBar()
        val orderKey = database.child("orders").push().key ?: return
        database.child("orders").child(orderKey).setValue(order)
        view.hideProgressBar()
        view.proceedToConfirmationScreen(orderKey)
    }

    private fun isAnyDataEmpty(street: String, city: String): Boolean {
        var anyEmpty = false

        if (street.isBlank()) {
            view.displayStreetErrorMessage("Street cannot be empty")
            anyEmpty = true
        }

        if (city.isBlank()) {
            view.displayCityErrorMessage("City cannot be empty")
            anyEmpty = true
        }

        return anyEmpty
    }

    interface View {
        fun displayStreetErrorMessage(message: String)
        fun displayCityErrorMessage(message: String)
        fun showProgressBar()
        fun hideProgressBar()
        fun proceedToConfirmationScreen(orderId: String)
    }
}