package com.szymanczyk.machnicki.burgerit.product

open class Product(
    val id: String,
    val name: String,
    val price: Double,
    val details: String
) {
    override fun toString() = "Product(id='$id', name='$name', price=$price, details='$details')"
}