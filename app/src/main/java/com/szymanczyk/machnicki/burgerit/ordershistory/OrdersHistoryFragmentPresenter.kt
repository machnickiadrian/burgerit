package com.szymanczyk.machnicki.burgerit.ordershistory

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.szymanczyk.machnicki.burgerit.cart.CartRecord
import com.szymanczyk.machnicki.burgerit.checkout.Client
import com.szymanczyk.machnicki.burgerit.checkout.Delivery
import com.szymanczyk.machnicki.burgerit.checkout.Order
import com.szymanczyk.machnicki.burgerit.product.Product
import java.time.LocalDateTime

class OrdersHistoryFragmentPresenter(val view: View) {

    private val database = FirebaseDatabase.getInstance().reference

    fun fetchOrdersHistory(userEmail: String) {
        view.showProgressBar()
        database.child("orders").addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                val ordersList = snapshot.children
                    .filter { it.child("client").child("email").value.toString() == userEmail }
                    .mapNotNull {
                        Order(
                            date = it.child("date").value.toString(),
                            client = getClientFromSnapshot(it),
                            products = getProductsFromSnapshot(it),
                            amount = it.child("amount").value.toString().toDouble(),
                            delivery = getDeliveryFromSnapshot(it),
                            location = it.child("location").value?.toString()
                        )
                    }
                    .sortedByDescending { LocalDateTime.parse(it.date) }
                    .toMutableList()

                if (ordersList.isNotEmpty()) {
                    view.displayOrdersHistory(ordersList)
                }
                else {
                    view.displayEmptyOrderHistoryInfo()
                }

                view.hideProgressBar()
            }

            override fun onCancelled(error: DatabaseError) {
                // do nothing
            }

        })

    }

    private fun getDeliveryFromSnapshot(it: DataSnapshot): Delivery? {
        return if (it.child("delivery").exists()) {
            Delivery(
                price = it.child("delivery").child("price").value.toString().toDouble(),
                name = it.child("delivery").child("name").value.toString(),
                street = it.child("delivery").child("street").value.toString(),
                city = it.child("delivery").child("city").value.toString()
            )
        } else {
            null
        }
    }

    private fun getProductsFromSnapshot(it: DataSnapshot): List<CartRecord> {
        return it.child("products").children
            .mapNotNull {
                CartRecord(
                    product = Product(
                        id = it.child("product").key.toString(),
                        name = it.child("product").child("name").value.toString(),
                        price = it.child("product").child("price").value.toString().toDouble(),
                        details = ""
                    ),
                    quantity = it.child("quantity").value.toString().toInt()
                )
            }
            .toList()
    }

    private fun getClientFromSnapshot(it: DataSnapshot): Client {
        return Client(
            name = it.child("client").child("name").value.toString(),
            email = it.child("client").child("email").value.toString(),
            phoneNumber = it.child("client").child("phoneNumber").value.toString()
        )
    }


    interface View {
        fun displayOrdersHistory(ordersList: MutableList<Order>)
        fun displayEmptyOrderHistoryInfo()
        fun showProgressBar()
        fun hideProgressBar()
    }
}