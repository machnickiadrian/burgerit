package com.szymanczyk.machnicki.burgerit.aboutus

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location

class AboutUsFragmentPresenter(val view: View) {

    private val database = FirebaseDatabase.getInstance().reference

    fun displayLocations() {
        view.showProgressBar()
        database.child("locations").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children
                    .mapNotNull {
                        Location(
                            id = it.key.toString(),
                            name = it.child("name").value.toString(),
                            city = it.child("city").value.toString(),
                            address = it.child("address").value.toString(),
                            phoneNumber = it.child("phoneNumber").value.toString(),
                            latitude = it.child("latitude").value.toString().toDouble(),
                            longitude = it.child("longitude").value.toString().toDouble()
                        )
                    }
                    .forEach { view.displayLocationOnMap(it) }

                view.hideProgressBar()
            }

            override fun onCancelled(error: DatabaseError) {
                // do nothing
            }

        })

    }

    fun selectLocation(location: Location, locationIndex: Int) {
        AppState.selectLocation(location, locationIndex)
        view.updateSelectLocationButtonText(location.name)
        view.moveMapToSelectedLocation(location)
        view.updateInfoFooter(location)
    }

    interface View {
        fun displayLocationOnMap(location: Location)
        fun updateSelectLocationButtonText(label: String)
        fun updateInfoFooter(location: Location)
        fun moveMapToSelectedLocation(location: Location)
        fun showProgressBar()
        fun hideProgressBar()
    }
}