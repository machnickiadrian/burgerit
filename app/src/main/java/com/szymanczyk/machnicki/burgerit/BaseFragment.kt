package com.szymanczyk.machnicki.burgerit

import android.support.v4.app.Fragment
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location

abstract class BaseFragment : Fragment() {
    abstract fun onLocationSelected(location: Location, locationIndex: Int)
}