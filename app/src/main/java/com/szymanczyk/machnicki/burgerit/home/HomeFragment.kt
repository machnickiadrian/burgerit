package com.szymanczyk.machnicki.burgerit.home

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.BaseFragment
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.aboutus.model.Location
import com.szymanczyk.machnicki.burgerit.menu.BURGERS
import com.szymanczyk.machnicki.burgerit.menu.DRINKS
import com.szymanczyk.machnicki.burgerit.menu.MenuFragment
import com.szymanczyk.machnicki.burgerit.menu.SNACKS
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment(), HomeFragmentPresenter.View {

    private lateinit var activity: MainActivity
    private lateinit var presenter: HomeFragmentPresenter

    private var locations = mutableListOf<Location>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MainActivity
        presenter = HomeFragmentPresenter(this)
        activity.updateViewTitle(getString(R.string.app_name))
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        layout_offer_burgers.setOnClickListener {
            activity.replaceFragment(MenuFragment.newInstance(BURGERS))
        }

        layout_offer_snacks.setOnClickListener {
            activity.replaceFragment(MenuFragment.newInstance(SNACKS))
        }

        layout_offer_drinks.setOnClickListener {
            activity.replaceFragment(MenuFragment.newInstance(DRINKS))
        }

        presenter.fetchLocations()

        btn_select_location.setOnClickListener {
            // TODO extract to one place
            val dialogBuilder = AlertDialog.Builder(activity)
            dialogBuilder.setTitle(getString(R.string.select_location))

            dialogBuilder.setSingleChoiceItems(
                locations.map { it.name }.toTypedArray(),
                AppState.getSelectedLocationIndex()
            ) { dialog, which ->
                Log.d("Selected location", locations[which].name)
                presenter.selectLocation(locations[which], which)
                dialog.dismiss()
            }

            val alertDialog = dialogBuilder.create()
            alertDialog.show()
        }
    }

    override fun onResume() {
        super.onResume()
        val selectedLocation = AppState.getSelectedLocation()
        if (selectedLocation != null) {
            updateSelectLocationButtonText(selectedLocation.name)
        }
    }

    override fun onLocationSelected(location: Location, locationIndex: Int) {
        presenter.onLocationSelected(location)
    }

    override fun addLocation(location: Location) {
        locations.add(location)
    }

    override fun updateSelectLocationButtonText(label: String) {
        btn_select_location.text = label
    }

    override fun showProgressBar() {
        activity.showProgressBar()
    }

    override fun hideProgressBar() {
        activity.hideProgressBar()
    }

    companion object {

        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}
