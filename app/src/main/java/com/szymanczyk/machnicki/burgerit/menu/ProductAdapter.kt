package com.szymanczyk.machnicki.burgerit.menu

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.szymanczyk.machnicki.burgerit.AppState
import com.szymanczyk.machnicki.burgerit.MainActivity
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.cart.AddToCartFragment
import com.szymanczyk.machnicki.burgerit.product.Burger
import com.szymanczyk.machnicki.burgerit.product.Product

class ProductAdapter(private var productList: MutableList<Product>, private var view: MenuFragment, private var menuType: String): RecyclerView.Adapter<MenuViewHolder>() {

    private var originalProductList: MutableList<out Product> = productList.toMutableList()

    private var activeSortTypes = mutableSetOf<ProductSortType>()
    private var activeFilterTypes = mutableSetOf<ProductFilterType>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.menu_item, parent, false)
        return MenuViewHolder(view)
    }

    override fun getItemCount() = productList.size

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val product = productList[position]
        holder.tvTitle.text = product.name
        holder.tvDetails.text = product.details
        holder.tvPrice.text = "%.2f".format(product.price)

        if (product is Burger) {
            if (product.spicy) holder.ivSpicyMark.visibility = View.VISIBLE
            if (product.vege) holder.ivVegeMark.visibility = View.VISIBLE
        }

        holder.root.setOnClickListener {
            AppState.productToAddToCart = product
            val fragment = AddToCartFragment.newInstance(menuType)
            val activity = view.activity as MainActivity
            activity.replaceFragment(fragment)
        }

        val activity = view.activity as MainActivity
        activity.hideProgressBar()
    }

    fun sortBy(sortType: ProductSortType, sort: Boolean = true) {
        if (sort) {
            activeSortTypes.add(sortType)
        } else {
            activeSortTypes.remove(sortType)
        }

        refreshView()
    }

    fun filterBy(filterType: ProductFilterType, filter: Boolean = true) {
        if (filter) {
            activeFilterTypes.add(filterType)
        } else {
            activeFilterTypes.remove(filterType)
        }

        refreshView()
    }

    private fun refreshView() {
        // TODO make it generic
        productList = originalProductList.toMutableList()

        if (activeSortTypes.isEmpty() && activeFilterTypes.isEmpty()) {
            notifyDataSetChanged()
            return
        }

        if (activeSortTypes.contains(ProductSortType.BY_PRICE_ASCENDING)) {
            productList.sortBy { it.price }
        }
        if (activeFilterTypes.contains(ProductFilterType.SPICY)) {
            productList = productList.filter { (it as Burger).spicy }.toMutableList()
        }
        if (activeFilterTypes.contains(ProductFilterType.VEGE)) {
            productList = productList.filter { (it as Burger).vege }.toMutableList()
        }

        notifyDataSetChanged()
    }

}

enum class ProductFilterType {
    SPICY,
    VEGE
}

enum class ProductSortType {
    BY_PRICE_ASCENDING
}