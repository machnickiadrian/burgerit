package com.szymanczyk.machnicki.burgerit.ordershistory

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.szymanczyk.machnicki.burgerit.R
import com.szymanczyk.machnicki.burgerit.checkout.Order
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class OrderHistoryRecordAdapter(private var orderHistoryList: MutableList<Order>, private var view: OrdersHistoryFragment): RecyclerView.Adapter<OrderHistoryRecordViewHolder>() {

    private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): OrderHistoryRecordViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.order_history_item, parent, false)
        return OrderHistoryRecordViewHolder(view)
    }

    override fun getItemCount() = orderHistoryList.size

    override fun onBindViewHolder(holder: OrderHistoryRecordViewHolder, position: Int) {
        val dateTimeText = LocalDateTime.parse(orderHistoryList[position].date)
            .format(dateTimeFormatter)
        holder.tvOrderDateTime.text = dateTimeText

        orderHistoryList[position].products
            .map { OrderHistoryProductItemView(view.context!!, it) }
            .forEach { holder.layoutProductList.addView(it) }

        holder.tvDeliveryPrice.text = if (orderHistoryList[position].delivery == null) {
            "-"
        } else {
            "${"%.2f".format(orderHistoryList[position].delivery!!.price)} zł"
        }
        holder.tvAmount.text = "${"%.2f".format(orderHistoryList[position].amount)} zł"
        holder.tvLocation.text = orderHistoryList[position].location ?: ""
    }

}

class OrderHistoryRecordViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var tvOrderDateTime: TextView = itemView.findViewById(R.id.tv_order_date_time)
    var layoutProductList: LinearLayout = itemView.findViewById(R.id.layout_product_list)
    var tvDeliveryPrice: TextView = itemView.findViewById(R.id.tv_delivery_price)
    var tvAmount: TextView = itemView.findViewById(R.id.tv_amount)
    var tvLocation: TextView = itemView.findViewById(R.id.tv_order_location)
}